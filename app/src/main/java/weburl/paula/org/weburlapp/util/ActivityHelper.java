package weburl.paula.org.weburlapp.util;

import android.app.*;
import android.content.DialogInterface;

import org.androidannotations.annotations.*;

import weburl.paula.org.weburlapp.R;

/**
 * Utility class
 */
@EBean
public class ActivityHelper {

    private ProgressDialog progressDialog;

    @RootContext
    protected Activity context;

    /**
     * Shows an exception thrown in a process
     *
     * @param ex
     */
    @UiThread
    public void showException(Exception ex) {
        String okButtonString = context.getString(android.R.string.ok);
        AlertDialog.Builder ad = new AlertDialog.Builder(context);
        ad.setTitle(context.getString(R.string.error));
        ad.setMessage(ex.toString());
        ad.setPositiveButton(okButtonString, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                context.finish();
            }
        });
        ad.show();
    }


    /**
     * Shows progress dialog with
     *
     * @param message
     * @param cancelable TODO
     * @param listener   TODO
     */
    @UiThread
    public void showProgress(int message, boolean cancelable,
            DialogInterface.OnCancelListener listener) {
        getProgressDialog().setTitle(message);
        if (! getProgressDialog().isShowing()) {
            getProgressDialog().setCancelable(cancelable);
            getProgressDialog().setOnCancelListener(listener);
            getProgressDialog().show();
        }
    }

    /**
     * Get or create if not exists a progress dialog
     * @return
     */
    public ProgressDialog getProgressDialog() {
        if (progressDialog == null) {
            if (context == null) {
                throw new UnsupportedOperationException(
                        "Progress dialog only supported on activities");
            }
            progressDialog = new ProgressDialog(context);
            progressDialog.setIndeterminate(true);
        }
        return progressDialog;
    }


    /**
     * Hide progress dialog (if any)
     */
    @UiThread
    public void hideProgress() {
        if (! getProgressDialog().isShowing()) {
            // Nothing to do
            return;
        }
        getProgressDialog().dismiss();
    }

}
