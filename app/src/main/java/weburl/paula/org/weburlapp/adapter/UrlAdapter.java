package weburl.paula.org.weburlapp.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.*;

/**
 * URL adapter
 */
public class UrlAdapter extends ArrayAdapter<String> {

    //Copy of the internal content
    private Collection<? extends String> urls;

    public UrlAdapter(Context context, int resource) {
        super(context, resource);
    }

    /**
     * Removes the content from the list
     */
    @Override
    public void clear() {
        super.clear();
        urls = new ArrayList<>();
    }

    /**
     * Add the objects into the list
     *
     * @param objects
     */
    @Override
    public void addAll(Collection<? extends String> objects)
    {
        super.addAll(objects);
        urls = objects;
    }

    /**
     * Get the urls list shown
     *
     * @return the urls list
     */
    public Collection<? extends String> getContent()
    {
        return urls;
    }
}
