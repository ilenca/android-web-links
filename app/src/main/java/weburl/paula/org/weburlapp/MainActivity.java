package weburl.paula.org.weburlapp;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ListView;

import org.androidannotations.annotations.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.*;
import java.util.*;

import weburl.paula.org.weburlapp.adapter.UrlAdapter;
import weburl.paula.org.weburlapp.util.ActivityHelper;

@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.menu_main)
public class MainActivity extends ActionBarActivity {

    private static final String LINKS = "links";

    /**
     * ListView to contain the URLs
     */
    @ViewById(R.id.listView)
    ListView listView;

    /**
     * Utility
     */
    @Bean
    ActivityHelper helper;

    /**
     * URLs Adapter
     */
    private UrlAdapter adapter;

    /**
     * Variable to avoid reload the URLs when activity is paused or has chandeg its orientation
     */
    boolean initialized = false;

    /**
     * Load UI changes and variables
     */
    @AfterViews
    void initUI() {

        // Create list view adapter
        adapter = new UrlAdapter(this, android.R.layout.simple_list_item_1);
        listView.setAdapter(adapter);
    }

    @Override
    public void onResume(){
        super.onResume();

        // When the activity is launched the URLs will be load.
        // It avoids reload the data when the activity is in background
        if(!initialized) {
            initialized = true;
            showUrls();
        }
    }


    /**
     * Send a request to the server to get the HTML of a webpage
     *
     * @param url of the server
     * @return html as a string
     * @throws IOException
     */
    protected String getHTML(String url) throws IOException {

        // Create a GET request
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        HttpResponse response = null;

        // Send request to the server
        response = client.execute(request);

        // Process response
        InputStream in = response.getEntity().getContent();

        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder str = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            str.append(line);
        }
        in.close();

        return  str.toString();
    }

    /**
     * Extract the URLs from the HTML code
     *
     * @param html code
     * @return list of URLs
     */
    private List<String> extractURLs(String html) {

        // Split the html by the character "<" to get all the sentences
        List<String> sentences = Arrays.asList(html.split("(?=<)"));
        List<String> links = new ArrayList<>();

        for (String line : sentences) {

            //Check if the sentence contains the "a" tag and "href" url
            if (line.contains("href") && line.contains("<a")) {

                // Extract the URL from the HTML sentence
                links.add(extractURL(line));
            }
        }

        return links;
    }

    /**
     * Get the URL from a HTML sentence
     *
     * @param line HTML
     * @return URL
     */
    private String extractURL(String line) {

        // The URL is after the href parameter
        line = line.split("href")[1];

        // Get the string
        line= line.split("\"")[1];

        // Delete blank spaces
        return line.trim();
    }

    /**
     * Update the UI URL list
     *
     * @param urls list
     */
    @UiThread
    protected void updateList(List<String> urls) {

        //Remove elements
        adapter.clear();

        //Add new url
        adapter.addAll(urls);
    }


    /**
     * Save  URLs to restore the state
     *
     * @param savedInstanceState
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the urls
        savedInstanceState.putStringArrayList(LINKS, (ArrayList<String>) adapter.getContent());

        super.onSaveInstanceState(savedInstanceState);
    }

    /**
     * Restore urls
     *
     * @param savedInstanceState
     */
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {

        super.onRestoreInstanceState(savedInstanceState);

        // Restore URL
        ArrayList<String> links = savedInstanceState.getStringArrayList(LINKS);

        // Avoid to reload the URL from the website
        initialized = true;

        // Update UI
        updateList(links);
    }


    /**
     * Get the URLS from the website and show them
     */
    @OptionsItem(R.id.action_update)
    @Background
    protected void showUrls() {

        try {

            //Show progress dialog
            helper.showProgress(R.string.load_url_progress, false, null);

            // Get the HTML code from the webpage
            String html = getHTML("http://www.visual-engin.com/Web/index.php");

            // Extract URLs from the HTML code
            List<String> urls = extractURLs(html);

            helper.hideProgress();

            // Set links in the UI
            updateList(urls);
        } catch (IOException e) {
            helper.showException(e);
        }
    }
}
